using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FIre : MonoBehaviour
{

    [SerializeField] private GameObject walk;

    private CreateMap map;
    private SpawnEnemy spawnEnemy;
    float x;
    float z;
    private bool spawn = true;

    private void Start()
    {
        x = transform.position.x;
        z = transform.position.z;
        map = FindObjectOfType<CreateMap>();
        spawnEnemy = FindObjectOfType<SpawnEnemy>();

    }

    private void OnTriggerEnter(Collider other)
    {

        walk.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        if (other)
        {

            Damageble damage;
            if (other.TryGetComponent<Damageble>(out damage))
            {
                damage.Death(other);
                damage.DestryWall(other);
                damage.EnemyDeath(other, spawnEnemy);
            }
            if (spawn)
            {
                GameObject tmp = Instantiate(walk);
                map.MapList[(int)x, (int)z] = tmp;
                spawn = false;
            }

        }

        StartCoroutine(TimerFire());
    }

    private IEnumerator TimerFire()
    {
        yield return new WaitForSeconds(0.5f);
        GameManager.Instance.playerController.BombOut = true;
        gameObject.SetActive(false);

    }
}

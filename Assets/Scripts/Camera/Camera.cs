using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{

    [SerializeField] private PlayerController player;
    [SerializeField] private float speedCamera = 10f;


    private void Update()
    {
        Vector3 dx = new Vector3(23.06f, transform.position.y, transform.position.z);
        Vector3 sx = new Vector3(10.89f, transform.position.y, transform.position.z);


        if (player.transform.position.x >= 21)
        {
            transform.position = Vector3.Lerp(transform.position,dx,speedCamera * Time.deltaTime);
        }
        else if (player.transform.position.x <= 21)
        {
            transform.position = Vector3.Lerp(transform.position, sx, speedCamera * Time.deltaTime);
        }
    }


}

using UnityEngine;
using UnityEngine.SceneManagement;

public class Damageble : MonoBehaviour
{

    public void Death(Collider other)
    {

        if (other.GetComponent<PlayerController>())
        {
            SceneManager.LoadScene("Loose");
        }

    }

    public void DestryWall(Collider other)
    {
        if (other)
        {
            other.gameObject.SetActive(false);
        }
    }

    public void EnemyDeath(Collider other, SpawnEnemy enemy)
    {


        if (other.GetComponent<Enemy>())
        {
            enemy.EnemyInMap--;
            other.gameObject.SetActive(false);
            GameManager.Instance.VictoryGame();
        }
    }

}

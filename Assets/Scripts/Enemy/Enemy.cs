using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private CreateMap map;


    [SerializeField] bool n1 = false;
    [SerializeField] bool n2 = false;
    [SerializeField] bool n3 = false;
    [SerializeField] bool n4 = false;

    private void Start()
    {
        map = FindObjectOfType<CreateMap>();

        float x = transform.position.x;
        float z = transform.position.z;
        Choice(map, x, z);


    }

    private void Update()
    {
        float x = transform.position.x;
        float z = transform.position.z;

        if (n1)
        {
            DX(map, x, z);
        }
        else if (n2)
        {
            SX(map, x, z);
        }


        if (n3)
        {
            Up(map, x, z);
        }
        else if (n4)
        {
            Down(map, x, z);
        }
    }

    public void Choice(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a + 1, (int)b].GetComponent<Walkable>() || map.MapList[(int)a - 1, (int)b].GetComponent<Walkable>())
        {
            n1 = true;
        }
        else if (map.MapList[(int)a, (int)b + 1].GetComponent<Block>() || map.MapList[(int)a, (int)b - 1].GetComponent<Block>())
        {
            n1 = true;
        }
        else if (map.MapList[(int)a + 1, (int)b].GetComponent<Block>() || map.MapList[(int)a + 1, (int)b].GetComponent<Block>())
        {
            n3 = true;
        }
        else if (map.MapList[(int)a, (int)b + 1].GetComponent<Walkable>() || map.MapList[(int)a, (int)b - 1].GetComponent<Walkable>())
        {
            n3 = true;
        }
    }




    public void DX(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a + 1, (int)b].GetComponent<Walkable>())
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else if (map.MapList[(int)a + 1, (int)b].GetComponent<Block>()
              || map.MapList[(int)a + 1, (int)b].GetComponent<Destroyed>())
        {
            if (map.MapList[(int)a, (int)b + 1].GetComponent<Walkable>())
            {
                n1 = false;
                n3 = true;

            }
            else if (map.MapList[(int)a, (int)b - 1].GetComponent<Walkable>())
            {
                n1 = false;
                n4 = true;

            }
            else
            {
                n1 = false;
                n2 = true;

            }

        }

    }

    public void SX(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a, (int)b].GetComponent<Walkable>())
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }
        else if (map.MapList[(int)a, (int)b].GetComponent<Block>()
              || map.MapList[(int)a, (int)b].GetComponent<Destroyed>())
        {
            n1 = true;
            n2 = false;
        }


    }

    public void Up(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a, (int)b + 1].GetComponent<Walkable>())
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
        else if (map.MapList[(int)a, (int)b + 1].GetComponent<Block>()
              || map.MapList[(int)a, (int)b + 1].GetComponent<Destroyed>())
        {
            if (map.MapList[(int)a +1 , (int)b].GetComponent<Walkable>())
            {
                n3 = false;
                n1 = true;

            }
            else if (map.MapList[(int)a -1, (int)b].GetComponent<Walkable>())
            {
                n3 = false;
                n2 = true;

            }
            else
            {

                n3 = false;
                n4 = true;

            }
        }

    }


    public void Down(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a, (int)b].GetComponent<Walkable>())
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
        else if (map.MapList[(int)a, (int)b].GetComponent<Block>()
              || map.MapList[(int)a, (int)b].GetComponent<Destroyed>())
        {
            n4 = false;
            n3 = true;
        }

    }


    private void OnTriggerEnter(Collider other)
    {

        Damageble damage;
        if (other.TryGetComponent<Damageble>(out damage))
        {
            damage.Death(other);
        }

    }

}

using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField] private InstaObj obj;
    [SerializeField] private CreateMap map;
    [SerializeField] private GameObject enemy;

    [SerializeField] private int numberOfEnemy = 5;
    [SerializeField] private int enemyInMap = 0;



    public int EnemyInMap { get => enemyInMap; set => enemyInMap = value; }



    private void Start()
    {

        GameManager.Instance.spawnEnemy = this;
        SpawnEn(map);
    }

    private void Update()
    {

    }

    public void SpawnEn(CreateMap map)
    {


        for (int i = 0; i < numberOfEnemy; i++)
        {

            int a = Random.Range(5, map.SizeX);
            int b = Random.Range(2, map.SizeZ);


            if (map.MapList[a, b].GetComponent<Walkable>())
            {
                enemy.transform.position = new Vector3(a, 1f, b);
                GameObject spawn = Instantiate(enemy);
                enemyInMap++;
            }
        }

    }
}

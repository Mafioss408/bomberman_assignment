using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CreateMap : MonoBehaviour
{
    [SerializeField] InstaObj insta;
    const int sizeX = 35;
    const int sizeZ = 13;

    private bool isEven;

    private GameObject[,] map = new GameObject[sizeX, sizeZ];

    private GameObject[,] mapRef = new GameObject[sizeX, sizeZ];

    [SerializeField] GameObject[] wallDestr = new GameObject[3];
    [SerializeField] GameObject block;
    [SerializeField] GameObject wall;
    [SerializeField] GameObject walkable;

 

    //property
    public GameObject[,] MapList { get => mapRef; set => value = mapRef; }
    public GameObject[,] MapTerrein { get => map; set => value = map; }
    public int SizeX { get => sizeX; }
    public int SizeZ { get => sizeZ; }


    private void Awake()
    {
        InitializeMatrix();
    }

    private void Start()
    {

        wallDestr[1] = null;
        GameManager.Instance.map = this;
    }

    private void InitializeMatrix()
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeZ; j++)
            {

                MapObstacle(i, j, this);
                insta.CreaTerrein(block,i,-0.5f,j,this);


            }
        }
    }


    private void MapObstacle(int a, int b, CreateMap map)
    {
        int c = a + b;

        if (a == 0 || b == 0 || a == sizeX - 1 || b == sizeZ - 1)
        {

            insta.CreaList(wall, a, 0.5f, b, map);
        }
        else if (a == 2 && b == 11)
        {
            insta.CreaList(walkable, a, 0.5f, b, map);
        }
        else if (a == 2 && b == 10)
        {
            insta.CreaList(walkable, a, 0.5f, b, map);
        }
        else if (a == 1 && b == 11)
        {
            insta.CreaList(walkable, a, 0.5f, b, map);
        }
        else if (a % 2 != 0 && c % 2 != 0)
        {
            insta.CreaList(wall, a, 0.5f, b, map);
        }

        else if (a > 0 || b > 0 || a < sizeX - 1 || b < sizeZ - 1 && mapRef[a, b] == null)
        {

            walkable.transform.position = new Vector3(a, 0.5f, b);
            GameObject _walkable = Instantiate(wallDestr[Random.Range(0, wallDestr.Length - 1)], walkable.transform.position, Quaternion.identity);

            mapRef[a, b] = _walkable.gameObject;
        }

    }
}


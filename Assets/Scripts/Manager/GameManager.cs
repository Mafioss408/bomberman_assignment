using UnityEngine;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{

    private static GameManager instance;

    public static GameManager Instance
    {
        get => instance;

        private set => instance = value;
    }


    private void Awake()
    {


        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        
    }



    public CreateMap map { get; set; }
    public PlayerController playerController { get; set; }
    public SpawnEnemy spawnEnemy { get; set; }


    private void Update()
    {

    }

    public void ChangLevel(string nameLevel)
    {
        SceneManager.LoadScene(nameLevel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void VictoryGame()
    {
        if (spawnEnemy.EnemyInMap == 0)
        {
            ChangLevel("Win");
        }

    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    private CreateMap map;
    [SerializeField] private GameObject fireSpawn;
    [SerializeField] private GameObject meshBomb;
    [SerializeField] private GameObject wallBomb;
    [SerializeField] private CheckBomb bomb;

    private bool spawn = true;

    private float x;
    private float z;

    private void Awake()
    {
    }
    private void Start()
    {

        map = FindObjectOfType<CreateMap>();
        x = transform.position.x;
        z = transform.position.z;

    }
    void Update()
    {
        Detonate();


    }

    private void Detonate()
    {
        StartCoroutine(timer());
    }

    private IEnumerator timer()
    {
        if (spawn)
        {
            wallBomb.transform.position = new Vector3((int)transform.position.x, 1f, (int)transform.position.z);
            GameObject wallBomb1 = Instantiate(wallBomb);
            map.MapList[(int)x, (int)z] = wallBomb1;
            spawn = false;
        }
        yield return new WaitForSeconds(3f);
        CheckTerrein(map, x, z);
        gameObject.SetActive(false);

    }

    public void CheckTerrein(CreateMap map, float a, float b)
    {

        bomb.CheckMapBomb(fireSpawn, (int)a, 0, (int)b, 0);

        if (map.MapList[(int)a + 1, (int)b].GetComponent<Walkable>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 1, (int)b, 0);
        }
        else if (map.MapList[(int)a + 1, (int)b].GetComponent<Destroyed>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 1, (int)b, 0);
        }
        if (map.MapList[(int)a - 1, (int)b].GetComponent<Walkable>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, -1, (int)b, 0);
        }
        else if (map.MapList[(int)a - 1, (int)b].GetComponent<Destroyed>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, -1, (int)b, 0);
        }
        if (map.MapList[(int)a, (int)b + 1].GetComponent<Walkable>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 0, (int)b, 1);
        }
        else if (map.MapList[(int)a, (int)b + 1].GetComponent<Destroyed>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 0, (int)b, 1);
        }
        if (map.MapList[(int)a, (int)b - 1].GetComponent<Walkable>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 0, (int)b, -1);
        }
        else if (map.MapList[(int)a, (int)b - 1].GetComponent<Destroyed>())
        {
            bomb.CheckMapBomb(fireSpawn, (int)a, 0, (int)b, -1);
        }
    }
}

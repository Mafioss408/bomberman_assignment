using UnityEngine;


[CreateAssetMenu(fileName = "CheckMap", menuName = "ScriptableObject/Bomb")]
public class CheckBomb : ScriptableObject
{ 
    public void CheckMapBomb(GameObject fire, int posX , int signeX, int PosZ, int signeZ)
    {
        fire.transform.position = new Vector3(posX + ( 1 * signeX), 0.5f, PosZ + ( 1 * signeZ));
        GameObject fireq = Instantiate(fire);

    }
}

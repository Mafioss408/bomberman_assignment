using System.Collections;
using UnityEngine;

public class CheckMap : MonoBehaviour
{
    [SerializeField ] Transform movePoint;
    [SerializeField] private float speed;

    private Animator animator; 

    #region Variable Bool ( Control move )

    private bool dx = false;
    private bool sx = false;
    private bool top = false;
    private bool down = false;

    private bool pressD = true;
    private bool pressA = true;
    private bool pressW = true;
    private bool pressS = true;

    private bool Go = true;

    #endregion


    private void Awake()
    {
        movePoint.parent = null;
        animator = GetComponentInChildren<Animator>();
    }
    private void Update()
    {
        transform.position = transform.position;
    }

    public void CheckMapMovemiment(CreateMap map, float a, float b)
    {
        float x = movePoint.transform.position.x;
        float z = movePoint.transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, movePoint.position) < 0.05f)
        {
            
            CheckDX(map, a, b);
            CheckSX(map, a, b);
            CheckTop(map, a, b);
            CheckDown(map, a, b);

            if (dx && Go)
            {

                if (Mathf.Abs(Input.GetAxisRaw("HorizontalRight")) == 1f && pressD && Go)
                {
                    Go= false;
                    pressD = false;
                    StartCoroutine(DalayRight());
                }
            }
            if (sx && Go)
            {
                
                if (Mathf.Abs(Input.GetAxisRaw("HorizontalLefth")) == 1f && pressA && Go)
                {
                    Go = false;
                    pressA = false;
                    StartCoroutine(DalayLefth());
                }

            }
            if (top && Go)
            {

                if (Mathf.Abs(Input.GetAxisRaw("VerticalTop")) == 1f && pressW && Go )
                {
                    Go = false;
                    pressW = false;
                    StartCoroutine(DalayTop());
                }
            }

            if (down && Go)
            {

                if (Mathf.Abs(Input.GetAxisRaw("VerticalDown")) == 1f && pressW && Go)
                {
                    Go = false;
                    pressS = false;
                    StartCoroutine(DalayDown());
                }
            }
        }

    }

    private void CheckDX(CreateMap map, float a, float b)
    {
        if (map.MapList[(int)a + 1, (int)b].GetComponent<Walkable>())
        {

            dx = true;

        }
        else
        {
            dx = false;
        }
    }
    private void CheckSX(CreateMap map, float a, float b)
    {

        if (map.MapList[(int)a - 1, (int)b].GetComponent<Walkable>())
        {
            sx = true;
        }
        else
        {
            sx = false;
        }


    }
    private void CheckTop(CreateMap map, float a, float b)
    {
     
        if (map.MapList[(int)a, (int)b + 1].GetComponent<Walkable>())
        {
            top = true;
        }
        else
        {
            top = false;
        }

    }
    private void CheckDown(CreateMap map, float a, float b)
    {
       
        if (map.MapList[(int)a, (int)b - 1].GetComponent<Walkable>())
        {
            down = true;
        }
        else
        {
            down = false;
        }
    }

    private IEnumerator DalayRight()
    {
        movePoint.position += new Vector3(Input.GetAxisRaw("HorizontalRight"), 0f, 0f);
        animator.SetBool("HIdeL", false);
        animator.SetBool("RunR", true);
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("RunR", false);
        pressD = true;
        Go= true;
    }
    private IEnumerator DalayLefth()
    {
        movePoint.position += new Vector3(Input.GetAxisRaw("HorizontalLefth"), 0f, 0f);
        animator.SetBool("RunL", true);
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("RunL", false);
        
;        pressA = true;
        Go = true;
    }
    private IEnumerator DalayTop()
    {
        movePoint.position += new Vector3(0f, 0f, Input.GetAxisRaw("VerticalTop"));
        yield return new WaitForSeconds(0.5f);
        pressW = true;
        Go = true;
    }
    private IEnumerator DalayDown()
    {
        movePoint.position += new Vector3(0f, 0f, Input.GetAxisRaw("VerticalDown"));
        yield return new WaitForSeconds(0.5f);
        pressS = true;
        Go = true;
    }
}

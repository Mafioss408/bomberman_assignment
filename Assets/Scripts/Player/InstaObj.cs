using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstaObj: MonoBehaviour
{
    public void CreaList (GameObject tmp , int a ,float c, int b, CreateMap map)
    {
        tmp.transform.position = new Vector3(a, c, b);
        GameObject spawn = Instantiate(tmp);
        map.MapList[a, b] = spawn.gameObject;
        spawn.transform.parent = transform;

    }
    public void CreaTerrein(GameObject tmp, int a, float c, int b, CreateMap map)
    {
        tmp.transform.position = new Vector3(a, c, b);
        GameObject spawn = Instantiate(tmp);
        map.MapTerrein[a, b] = spawn.gameObject;
        spawn.transform.parent = transform;
    }
}

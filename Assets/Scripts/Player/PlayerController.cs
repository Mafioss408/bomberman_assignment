using UnityEngine;

public class PlayerController : CheckMap
{

    [SerializeField] private CreateMap map;
    [SerializeField] private GameObject bomb;

 
    private float x;
    private float z;
    private bool bombOut = true;

    public bool BombOut { get { return bombOut; } set { bombOut = value; } }

    private void Start()
    {
        GameManager.Instance.playerController = this;
    }

    void Update()
    {
        x = transform.position.x;
        z = transform.position.z;

        CheckMapMovemiment(map, x, z);
        Bomb();
    }

    private void Bomb()
    {
        Vector3 bombSpawn = new Vector3((int)transform.position.x, 0.5f, (int)transform.position.z);

        if (bombOut)
        {
            if (Input.GetButtonDown("Jump"))
            {
                bombOut = false;
                GameObject _bomb = Instantiate(bomb, bombSpawn, Quaternion.identity);
            }
        }
    }

}
